package fr.adaming.auth;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IAgentDao;
import fr.adaming.model.Agent;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	// transformation de l'association uml en java
	@Autowired
	private IAgentDao agDao;
	@Autowired
	private PasswordEncoder bcryptEncoder;

	// Si le username  est present dans la base de donnees alors
    // elle retourne un objet user avec en password le hash correspondant au mot de passe
    // récupéré dans la bd

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// récupérer la liste des agents enregistrés dans la bd
		List<Agent> listeAgents = agDao.findAll();
		
		// on cherche le login saisi par l'utilisateur dans la base de données
		boolean isInDatabase = false;
		Agent agentConnecte = new Agent();
		for (Agent agent : listeAgents) {
			if (agent.getLogin().equals(username)) {
				isInDatabase=true;
				agentConnecte = agent;
				break;
			}
		}
		
		if (isInDatabase) {
			return new User(agentConnecte.getLogin(), agentConnecte.getMdp(), new ArrayList<>());
			
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
	
	// ajouter un agent dans la bd en cryptant son mdp
	public Agent saveAgent(Agent agent) {
		agent.setMdp(bcryptEncoder.encode(agent.getMdp()));
		return agDao.save(agent);
	}

}
