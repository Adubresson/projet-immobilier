package fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.Client;
import fr.adaming.model.Contrat;
import fr.adaming.service.IBienAAcheterService;
import fr.adaming.service.IBienALouerService;
import fr.adaming.service.IContratService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/contrat")
public class ContratRest {

	// transformation de l'association UML en java
	@Autowired
	private IContratService contratService;
	@Autowired
	private IBienALouerService balService;
	@Autowired
	private IBienAAcheterService baaService;

	@GetMapping(value = "/list", produces = "application/json")
	public List<Contrat> getAllContrats() {
		return contratService.getAllContrat();
	}

	@GetMapping(value = "/find/{pId}", produces = "application/json")
	public Contrat getContratById(@PathVariable("pId") int id) {

		return contratService.getContratById(id);
	}

	@PostMapping(value = "/add/{idCl}/{idBien}", consumes = "application/json", produces = "application/json")
	public Contrat addContrat(@RequestBody Contrat contrat, @PathVariable("idCl") int idCl,
			@PathVariable("idBien") int idBien) {
		// instancier un client associé à la visite et lui associer l'id récupéré
		contrat.setClient(new Client());
		contrat.getClient().setId(idCl);

		// chercher le bien dans la table des biens à louer
		if (idBien != 0) {
			BienALouer balOut = balService.searchById(idBien);

			if (balOut != null) {
				contrat.setBal(new BienALouer(idBien));
			} else {
				// chercher le bien dans la table des biens à acheter
				BienAAcheter baaOut = baaService.searchById(idBien);

				if (baaOut != null) {
					contrat.setBaa(new BienAAcheter(idBien));
				}
			}
		}

		return contratService.addContrat(contrat);
	}

	@PutMapping(value = "/update/{idCl}/{idBien}", consumes = "application/json", produces = "application/json")
	public Contrat updateContrat(@RequestBody Contrat contrat, @PathVariable("idCl") int idCl,
			@PathVariable("idBien") int idBien) {
		// instancier un client associé à la visite et lui associer l'id récupéré
		contrat.setClient(new Client());
		contrat.getClient().setId(idCl);

		// chercher le bien dans la table des biens à louer
		if (idBien != 0) {
			BienALouer balOut = balService.searchById(idBien);

			if (balOut != null) {
				contrat.setBal(new BienALouer(idBien));
			} else {
				// chercher le bien dans la table des biens à acheter
				BienAAcheter baaOut = baaService.searchById(idBien);

				if (baaOut != null) {
					contrat.setBaa(new BienAAcheter(idBien));
				}
			}
		}
		return contratService.updateContrat(contrat);
	}

	@DeleteMapping(value = "/del/{pId}")
	public void deleteContrat(@PathVariable("pId") int id) {

		contratService.deleteContrat(id);
	}

	@GetMapping(value = "/dashBal/{pId}", produces = "application/json")
	public List<BienALouer> getListBalAcquis(@PathVariable("pId") int id) {
		if (contratService.getListBalAcquis(id) != null) {
			return contratService.getListBalAcquis(id);
		} else {
			return null;
		}
	}

	@GetMapping(value = "/dashBaa/{pId}", produces = "application/json")
	public List<BienAAcheter> getListBaaAcquis(@PathVariable("pId") int id) {
		if (contratService.getListBaaAcquis(id) != null) {
			return contratService.getListBaaAcquis(id);
		} else {
			return null;
		}
	}
	
	@GetMapping(value="/getRev/{pId}", produces="application/json")
	public Double getRevenu(@PathVariable("pId") int id) {
		return contratService.getRevenu(id);
	}

}
