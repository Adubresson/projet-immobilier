package fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.ClasseStandard;
import fr.adaming.service.IClasseStrandardService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/clS")
public class ClasseStandardRest {

	// transformation UML en java
	@Autowired
	private IClasseStrandardService clStandardService;

	@GetMapping(value = "/list", produces = "application/json")
	public List<ClasseStandard> getAllClasseStandard() {
		return clStandardService.getAllClasseStandard();
	}

	@GetMapping(value = "/find/{pId}", produces = "application/json")
	public ClasseStandard getClasseStrandardById(@PathVariable("pId") int id) {
		return clStandardService.getClasseStrandardById(id);

	}

	@PostMapping(value = "/add", consumes = "application/json", produces = "application/json")
	public ClasseStandard addClasseStrandard(@RequestBody ClasseStandard classeS) {
		return clStandardService.addClasseStrandard(classeS);
	}

	@PutMapping(value = "/update", consumes = "application/json", produces = "application/json")
	public ClasseStandard updateClasseStrandard(@RequestBody ClasseStandard classeS) {
		return clStandardService.updateClasseStrandard(classeS);
	}

	@DeleteMapping(value = "/del/{pId}")
	public void deleteClasseStrandard(@PathVariable("pId") int id) {
		clStandardService.deleteClasseStrandard(id);
	}
}
