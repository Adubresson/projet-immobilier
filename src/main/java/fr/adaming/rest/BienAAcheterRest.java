package fr.adaming.rest;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.BienAAcheter;
import fr.adaming.model.ClasseStandard;
import fr.adaming.model.Image;
import fr.adaming.model.Proprietaire;
import fr.adaming.service.IBienAAcheterService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/baa")
public class BienAAcheterRest {

	// Transformation de l'association UML en Java
	@Autowired
	private IBienAAcheterService baaService;

	@GetMapping(value = "/list", produces = "application/json")
	public List<BienAAcheter> recupListe() {
		return baaService.getAll();
	}

	@GetMapping(value = "/getBaaCs/{idCs}", produces = "application/json")
	public List<BienAAcheter> recupListeByCs(@PathVariable("idCs") int idCs) {
		return baaService.getBienImoAA(idCs);
	}
	
	@GetMapping(value = "/getBaaCsDispos/{idCs}", produces = "application/json")
	public List<BienAAcheter> recupListeByCsDispos(@PathVariable("idCs") int idCs) {
		return baaService.getBienImoAADispos(idCs);
	}

	@PostMapping(value = "/add/{idProprio}/{idCs}", consumes = "application/json", produces = "application/json")
	public BienAAcheter ajouterBAL(@RequestBody BienAAcheter baaIn, @PathVariable("idProprio") int idProprio,
			@PathVariable("idCs") int idCs) {
		// instancier le proprietaire associé au bien et lui associer l'id récupéré en
		// param
		baaIn.setProprietaire(new Proprietaire());
		baaIn.getProprietaire().setId(idProprio);

		// idem pour la classe standard
		baaIn.setClasseStandard(new ClasseStandard());
		baaIn.getClasseStandard().setId(idCs);

		// transformer les images (String) en photos (Blob)
		for (Image img : baaIn.getImages()) {
			img.setPhoto(Base64.decodeBase64(img.getImage()));

		}

		return baaService.add(baaIn);
	}

	@PutMapping(value = "/update/{idProprio}/{idCs}", consumes = "application/json")
	public BienAAcheter modifierBAL(@RequestBody BienAAcheter baaIn, @PathVariable("idProprio") int idProprio,
			@PathVariable("idCs") int idCs) {
		// instancier le proprietaire associé au bien et lui associer l'id récupéré en
		// param
		baaIn.setProprietaire(new Proprietaire());
		baaIn.getProprietaire().setId(idProprio);

		// idem pour la classe standard
		baaIn.setClasseStandard(new ClasseStandard());
		baaIn.getClasseStandard().setId(idCs);


		// transformer l'image (String) en photos (Blob)

		return baaService.update(baaIn);
	}

	@GetMapping(value = "/find/{pId}", produces = "application/json")
	public BienAAcheter chercherBAL(@PathVariable("pId") int id) {
		return baaService.searchById(id);
	}

	@DeleteMapping(value = "/del/{pId}")
	public void supprimerBAL(@PathVariable("pId") int id) {
		baaService.delete(id);
	}
	

}
