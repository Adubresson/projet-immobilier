package fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.Proprietaire;
import fr.adaming.service.IProprietaireService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/proprietaire")
public class ProprietaireRest {
	
	// transformation de l'association uml en java
	@Autowired
	private IProprietaireService propService;
	
	@GetMapping(value="/list", produces="application/json")
	public List<Proprietaire> getAllProprietaires() {
		return propService.getAllProprietaires();
	}
	
	@GetMapping(value="/find/{pId}", produces="application/json")
	public Proprietaire getProprietaireById(@PathVariable("pId") int id) {
		return propService.getProprietaireById(id);
	}
	
	@PostMapping(value="/add", consumes="application/json", produces="application/json")
	public Proprietaire addProprietaire(@RequestBody Proprietaire proprietaire) {
		return propService.addProprietaire(proprietaire);
	}
	
	@PutMapping(value="/update", consumes="application/json", produces="application/json")
	public Proprietaire updateProprietaire(@RequestBody Proprietaire proprietaire) {
		return propService.updateProprietaire(proprietaire);
	}
	
	@DeleteMapping(value="/del/{pId}")
	public void deleteProprietaire(@PathVariable("pId") int id) {
		propService.deleteProprietaire(id);
	}

}
