package fr.adaming.rest;

import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.ClasseStandard;
import fr.adaming.model.Image;
import fr.adaming.model.Proprietaire;
import fr.adaming.service.IBienALouerService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/bal")
public class BienALouerRest {

	// Transformation de l'association UML en Java
	@Autowired
	private IBienALouerService balService;

	@GetMapping(value = "/list", produces = "application/json")
	public List<BienALouer> recupListe() {
		return balService.getAll();
	}

	@GetMapping(value = "/getBalCs/{idCs}", produces = "application/json")
	public List<BienALouer> recupListeByCs(@PathVariable("idCs") int idCs) {
		return balService.getBienImoAL(idCs);
	}
	
	@GetMapping(value = "/getBalCsDispos/{idCs}", produces = "application/json")
	public List<BienALouer> recupListeByCsDispos(@PathVariable("idCs") int idCs) {
		return balService.getBienImoALDispos(idCs);
	}

	@PostMapping(value = "/add/{idProprio}/{idCs}", consumes = "application/json", produces = "application/json")
	public BienALouer ajouterBAL(@RequestBody BienALouer balIn, @PathVariable("idProprio") int idProprio,
			@PathVariable("idCs") int idCs) {
		// instancier le proprietaire associé au bien et lui associer l'id récupéré en
		// param
		balIn.setProprietaire(new Proprietaire());
		balIn.getProprietaire().setId(idProprio);

		// idem pour la classe standard
		balIn.setClasseStandard(new ClasseStandard());
		balIn.getClasseStandard().setId(idCs);

		// transformer les images (String) en photos (Blob)
		for (Image img : balIn.getImages()) {
			img.setPhoto(Base64.decodeBase64(img.getImage()));

		}
		return balService.add(balIn);
	}

	@PutMapping(value = "/update/{idProprio}/{idCs}", consumes = "application/json")
	public BienALouer modifierBAL(@RequestBody BienALouer balIn, @PathVariable("idProprio") int idProprio,
			@PathVariable("idCs") int idCs) {
		// instancier le proprietaire associé au bien et lui associer l'id récupéré en
		// param
		balIn.setProprietaire(new Proprietaire());
		balIn.getProprietaire().setId(idProprio);

		// idem pour la classe standard
		balIn.setClasseStandard(new ClasseStandard());
		balIn.getClasseStandard().setId(idCs);

		return balService.update(balIn);
	}

	@GetMapping(value = "/find/{pId}", produces = "application/json")
	public BienALouer chercherBAL(@PathVariable("pId") int id) {
		return balService.searchById(id);
	}

	@DeleteMapping(value = "/del/{pId}")
	public void supprimerBAL(@PathVariable("pId") int id) {
		balService.delete(id);
	}
	


}
