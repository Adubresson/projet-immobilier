package fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.Agent;
import fr.adaming.model.ClasseStandard;
import fr.adaming.model.Client;
import fr.adaming.service.IClientService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/client")
public class ClientRest {
	
	// transformation de l'association uml en java
	@Autowired
	private IClientService clService;
	
	@GetMapping(value="/list", produces="application/json")
	public List<Client> getAllClients() {
		return clService.getAllClients();
	}
	
	@GetMapping(value="/find/{pId}", produces="application/json")
	public Client getClientById(@PathVariable("pId") int id) {
		return clService.getClientById(id);
	}
	
	@PostMapping(value="/add/{idAgent}", consumes="application/json", produces="application/json")
	public Client addClient(@RequestBody Client client,@PathVariable("idAgent") int idAgent) {
		// instancier l'agent associé au client et lui associer l'id récupéré
		client.setAgent(new Agent());
		client.getAgent().setId(idAgent);
	
		return clService.addClient(client);
	}
	
	@PutMapping(value="/update/{idAgent}", consumes="application/json", produces="application/json")
	public Client updateClient(@RequestBody Client client, @PathVariable("idAgent") int idAgent) {
		// instancier l'agent associé au client et lui associer l'id récupéré
		client.setAgent(new Agent());
		client.getAgent().setId(idAgent);
		
		return clService.updateClient(client);
	}
	
	@DeleteMapping(value="/del/{pId}")
	public void deleteClient(@PathVariable("pId") int id) {
		clService.deleteClient(id);
	}
	
	@GetMapping(value = "/cliClS/{pId}", produces = "application/json")
	public List<Client> getListClientByClS(@PathVariable("pId") int id){
		return clService.getListClientByClS(id);
	}

}
