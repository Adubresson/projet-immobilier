package fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.Client;
import fr.adaming.model.Visite;
import fr.adaming.service.IBienAAcheterService;
import fr.adaming.service.IBienALouerService;
import fr.adaming.service.IVisiteService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/visite")
public class VisiteRest {

	// transformation uml en java
	@Autowired
	private IVisiteService visiteService;
	@Autowired
	private IBienALouerService balService;
	@Autowired
	private IBienAAcheterService baaService;

	@GetMapping(value = "/list", produces = "application/json")
	public List<Visite> getAllVisite() {
		return visiteService.getAllVisite();
	}

	@GetMapping(value = "/find/{pId}", produces = "application/json")
	public Visite getVisiteById(@PathVariable("pId") int id) {
		return visiteService.getVisiteById(id);
	}

	@PostMapping(value = "/add/{idCl}/{idBien}", consumes = "application/json", produces = "application/json")
	public Visite addVisite(@RequestBody Visite visite, @PathVariable("idCl") int idCl,
			@PathVariable("idBien") int idBien) {
		// instancier un client associé à la visite et lui associer l'id récupéré
		visite.setClient(new Client());
		visite.getClient().setId(idCl);

		// chercher le bien dans la table des biens à louer
		if (idBien != 0) {
			BienALouer balOut = balService.searchById(idBien);

			if (balOut != null) {
				visite.setBal(new BienALouer(idBien));
			} else {
				// chercher le bien dans la table des biens à acheter
				BienAAcheter baaOut = baaService.searchById(idBien);

				if (baaOut != null) {
					visite.setBaa(new BienAAcheter(idBien));
				}
			}
		}

		return visiteService.addVisite(visite);
	}

	@PutMapping(value = "/update/{idCl}/{idBien}", consumes = "application/json", produces = "application/json")
	public Visite updateVisite(@RequestBody Visite visite, @PathVariable("idCl") int idCl,
			@PathVariable("idBien") int idBien) {
		
		// instancier un client associé à la visite et lui associer l'id récupéré
		visite.setClient(new Client());
		visite.getClient().setId(idCl);
		
		// chercher le bien dans la table des biens à louer
		if (idBien != 0) {
			BienALouer balOut = balService.searchById(idBien);

			if (balOut != null) {
				visite.setBal(new BienALouer(idBien));
			} else {
				// chercher le bien dans la table des biens à acheter
				BienAAcheter baaOut = baaService.searchById(idBien);

				if (baaOut != null) {
					visite.setBaa(new BienAAcheter(idBien));
				}
			}
		}
		
		return visiteService.updateVisite(visite);
	}

	@DeleteMapping(value = "/del/{pId}")
	public void deleteVisite(@PathVariable("pId") int id) {
		visiteService.deleteVisite(id);

	}
	
	@GetMapping(value="/get/{idAgent}", produces="application/json")
	public List<Visite> getVisitesByAgent(@PathVariable("idAgent") int idAgent) {
		return visiteService.getVisitesByAgent(idAgent);
	}

}
