package fr.adaming.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.adaming.model.Agent;
import fr.adaming.service.IAgentService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/agent")
public class AgentRest {
	
	// transformation de l'association uml en java
	@Autowired
	private IAgentService agService;
	
	@GetMapping(value="/list", produces="application/json")
	public List<Agent> getAllAgents() {
		return agService.getAllAgents();
	}
	
	@GetMapping(value="/find/{pId}", produces="application/json")
	public Agent getAgentById(@PathVariable("pId") int id) {
		return agService.getAgentById(id);
	}
	
	@PostMapping(value="/add", consumes="application/json", produces="application/json")
	public Agent addAgent(@RequestBody Agent agent) {
		return agService.addAgent(agent);
	}
	
	@PutMapping(value="/update", consumes="application/json", produces="application/json")
	public Agent updateAgent(@RequestBody Agent agent) {
		return agService.updateAgent(agent);
	}
	
	@DeleteMapping(value="/del/{pId}")
	public void deleteAgent(@PathVariable("pId") int id) {
		agService.deleteAgent(id);
	}
	
	@GetMapping(value="/get/{pLogin}", produces="application/json")
	public Agent getAgentByLogin(@PathVariable("pLogin") String login) {
		return agService.getAgentByLogin(login);
	}

}
