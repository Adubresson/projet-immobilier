package fr.adaming.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.adaming.model.Contrat;

@Repository
public interface IContratDao extends JpaRepository<Contrat, Integer>{

	@Query(value="SELECT contrats.bal_id FROM clients, contrats, agents WHERE agents.id_a=?1 AND agents.id_a=clients.a_id AND clients.id_p=contrats.cl_id", nativeQuery=true)
	public List<Integer> getListBalAcquis(int id);
	
	@Query(value="SELECT contrats.baa_id FROM clients, contrats, agents WHERE agents.id_a=?1 AND agents.id_a=clients.a_id AND clients.id_p=contrats.cl_id", nativeQuery=true)
	public List<Integer> getListBaaAcquis(int id);
	
	@Query(value="SELECT SUM(co.frais_agence) FROM contrats as co,clients as cl,agents as a WHERE a.id_a =?1 AND a.id_a=cl.a_id AND cl.id_p=co.cl_id", nativeQuery=true)
	public Double getRevenu(int id);
	
	@Query(value="SELECT contrats.* FROM contrats, biens WHERE biens.id_b=?1 AND contrats.bal_id=biens.id_b ", nativeQuery=true)
	public Contrat getContByIdBal(int id);
	
	@Query(value="SELECT contrats.* FROM contrats, biens WHERE biens.id_b=?1 AND contrats.baa_id=biens.id_b ", nativeQuery=true)
	public Contrat getContByIdBaa(int id);
	
	
	
}
