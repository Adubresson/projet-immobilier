package fr.adaming.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.adaming.model.Agent;
import fr.adaming.model.Visite;

@Repository
public interface IVisiteDao extends JpaRepository<Visite, Integer>{
	
	@Query("SELECT v FROM Visite v, Client cl, Agent ag WHERE v.client.id=cl.id AND cl.agent.id=ag.id AND ag.id=?1 ORDER BY v.date")
	public List<Visite> searchVisitesByAgent(int idAgent);

}
