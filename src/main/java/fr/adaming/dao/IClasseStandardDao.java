package fr.adaming.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.adaming.model.ClasseStandard;

public interface IClasseStandardDao extends JpaRepository<ClasseStandard, Integer> {
	
	

}
