package fr.adaming.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.adaming.model.Agent;


@Repository
public interface IAgentDao extends JpaRepository<Agent, Integer>{
	
	public Agent findByLogin(String login);

}
