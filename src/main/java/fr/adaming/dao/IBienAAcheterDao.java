package fr.adaming.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.adaming.model.BienAAcheter;

@Repository
public interface IBienAAcheterDao extends JpaRepository<BienAAcheter, Integer> {

	@Query(value="SELECT * FROM biens as baa WHERE baa.cs_id=?1 and baa.discr='ach'",nativeQuery = true)
	public List<BienAAcheter> getBienImoAA(int idCs);
	
	@Query(value="SELECT * FROM biens as baa WHERE baa.cs_id=?1 and baa.discr='ach' and baa.statut='disponible'",nativeQuery = true)
	public List<BienAAcheter> getBienImoAADispos(int idCs);
	

}