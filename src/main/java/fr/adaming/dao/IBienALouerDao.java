package fr.adaming.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;

@Repository
public interface IBienALouerDao extends JpaRepository<BienALouer, Integer> {

	
	@Query(value=" SELECT * FROM Biens as bal WHERE bal.cs_id=?1 and bal.discr='loc'",nativeQuery = true)
	public List<BienALouer> getBienImoAL(int idCs);
	
	@Query(value=" SELECT * FROM Biens as bal WHERE bal.cs_id=?1 and bal.discr='loc' and bal.statut='disponible'",nativeQuery = true)
	public List<BienALouer> getBienImoALDispos(int idCs);
	

}
