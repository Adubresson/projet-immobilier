package fr.adaming.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.adaming.model.Client;

@Repository
public interface IClientDao extends JpaRepository<Client, Integer> {
	
	
	@Query(value=" SELECT clients.id_p FROM clients, tab_assoc as ta, classes_standard as cls WHERE cls.id_cs =?1 AND cls.id_cs=ta.cs_id AND ta.p_id=clients.id_p",nativeQuery=true )
	public List<Integer> getListClientByClS(int id);

}
