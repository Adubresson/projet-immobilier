package fr.adaming.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.adaming.model.Proprietaire;

public interface IProprietaireDao extends JpaRepository<Proprietaire, Integer> {

}
