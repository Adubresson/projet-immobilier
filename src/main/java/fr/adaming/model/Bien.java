package fr.adaming.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="biens")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discr")
@XmlRootElement
@JsonIgnoreProperties({ "listVisites"})
//@MappedSuperclass
public class Bien implements Serializable {

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_b")
	protected int id;
	protected String statut; // à changer après ajout de Statut
	protected String type;
	protected Date dateSoumission;
	protected Date dateDisponibilite;
	protected double revenuCadastral;
	
	// transformation de l'association uml en java
	@ManyToOne
	@JoinColumn(name="p_id", referencedColumnName="id_p")
	protected Proprietaire proprietaire;
	@Embedded
	protected Adresse adresse;
	
	/**
	@OneToMany(mappedBy="bien")
	protected List<Visite> listVisites;
	**/
	@ManyToOne
	@JoinColumn(name="cs_id", referencedColumnName="id_cs")
	protected ClasseStandard classeStandard;
	
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	protected List<Image> images;
	
	// Constructeurs
	protected Bien() {
		super();
	}
	
	protected Bien(int id) {
		super();
		this.id = id;
	}
	
	protected Bien(String statut, Adresse adresse, String type, Date dateSoumission,
			Date dateDisponibilite, double revenuCadastral) {
		super();
		this.statut = statut;
		this.adresse = adresse;
		this.type = type;
		this.dateSoumission = dateSoumission;
		this.dateDisponibilite = dateDisponibilite;
		this.revenuCadastral = revenuCadastral;
	}
	
	protected Bien(int id, String statut, Adresse adresse, String type, Date dateSoumission,
			Date dateDisponibilite, double revenuCadastral) {
		super();
		this.id = id;
		this.statut = statut;
		this.adresse = adresse;
		this.type = type;
		this.dateSoumission = dateSoumission;
		this.dateDisponibilite = dateDisponibilite;
		this.revenuCadastral = revenuCadastral;
	}

	// Setters & Getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDateSoumission() {
		return dateSoumission;
	}

	public void setDateSoumission(Date dateSoumission) {
		this.dateSoumission = dateSoumission;
	}

	public Date getDateDisponibilite() {
		return dateDisponibilite;
	}

	public void setDateDisponibilite(Date dateDisponibilite) {
		this.dateDisponibilite = dateDisponibilite;
	}

	public double getRevenuCadastral() {
		return revenuCadastral;
	}

	public void setRevenuCadastral(double revenuCadastral) {
		this.revenuCadastral = revenuCadastral;
	}
	
	public Proprietaire getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(Proprietaire proprietaire) {
		this.proprietaire = proprietaire;
	}

	/**
	public List<Visite> getListVisites() {
		return listVisites;
	}

	public void setListVisites(List<Visite> listVisites) {
		this.listVisites = listVisites;
	}
	**/
	public ClasseStandard getClasseStandard() {
		return classeStandard;
	}

	public void setClasseStandard(ClasseStandard classeStandard) {
		this.classeStandard = classeStandard;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	@Override
	public String toString() {
		return "Bien [id=" + id + ", statut=" + statut + ", adresse=" + adresse + ", type=" + type + ", dateSoumission=" + dateSoumission + ", dateDisponibilite="
				+ dateDisponibilite + ", revenuCadastral=" + revenuCadastral + "]";
	}

	
	
	
	

}
