package fr.adaming.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="agents")
@XmlRootElement
@JsonIgnoreProperties({"listClients"})
public class Agent implements Serializable {

	//déclaration des attributs
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_a")
	private int id;
	private String nom;
	private String login;
	private String mdp;
	private boolean active;
	
	// transformation de l'association uml en java
	@OneToMany(mappedBy="agent")
	private List<Client> listClients;
	
	//constructeurs vides
	public Agent() {
		super();
	}

	public Agent(String nom, String login, String mdp, boolean active) {
		super();
		this.nom = nom;
		this.login = login;
		this.mdp = mdp;
		this.active = active;
	}

	public Agent(int id, String nom, String login, String mdp, boolean active) {
		super();
		this.id = id;
		this.nom = nom;
		this.login = login;
		this.mdp = mdp;
		this.active = active;
	}

	//les getters et setters 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	
	public List<Client> getListClients() {
		return listClients;
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	//redéfinition de toString
	@Override
	public String toString() {
		return "Agent [id=" + id + ", nom=" + nom + ", login=" + login + ", mdp=" + mdp + ", active=" + active + "]";
	}
	
	
	
}
