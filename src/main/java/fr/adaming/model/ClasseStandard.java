package fr.adaming.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="classes_standard")
@XmlRootElement
@JsonIgnoreProperties({"listBAA", "listBAL", "listClients"})
public class ClasseStandard implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cs")
	private int id;
	private String type;
	private String offre;
	private double prixMax;
	private String superficieMax;
	
	// transformation de l'association uml en java
	@OneToMany(mappedBy="classeStandard")
	private List<BienALouer> listBAL;
	@OneToMany(mappedBy="classeStandard")
	private List<BienAAcheter> listBAA;
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@JoinTable(name="tab_assoc", joinColumns=@JoinColumn(name="cs_id"), inverseJoinColumns=@JoinColumn(name="p_id"))
	private List<Client> listClients;

	public ClasseStandard(int id, String type, String offre, double prixMax, String superficieMax) {
		super();
		this.id = id;
		this.type = type;
		this.offre = offre;
		this.prixMax = prixMax;
		this.superficieMax = superficieMax;
	}

	public ClasseStandard() {
		super();
	}

	public ClasseStandard(String type, String offre, double prixMax, String superficieMax) {
		super();
		this.type = type;
		this.offre = offre;
		this.prixMax = prixMax;
		this.superficieMax = superficieMax;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOffre() {
		return offre;
	}

	public void setOffre(String offre) {
		this.offre = offre;
	}

	public double getPrixMax() {
		return prixMax;
	}

	public void setPrixMax(double prixMax) {
		this.prixMax = prixMax;
	}

	public String getSuperficieMax() {
		return superficieMax;
	}

	public void setSuperficieMax(String superficieMax) {
		this.superficieMax = superficieMax;
	}



	public List<BienALouer> getListBAL() {
		return listBAL;
	}

	public void setListBAL(List<BienALouer> listBAL) {
		this.listBAL = listBAL;
	}

	public List<BienAAcheter> getListBAA() {
		return listBAA;
	}

	public void setListBAA(List<BienAAcheter> listBAA) {
		this.listBAA = listBAA;
	}

	public List<Client> getListClients() {
		return listClients;
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}

	@Override
	public String toString() {
		return "ClasseStandard [id=" + id + ", type=" + type + ", offre=" + offre + ", prixMax=" + prixMax
				+ ", superficieMax=" + superficieMax + "]";
	}

}
