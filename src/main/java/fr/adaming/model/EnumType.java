package fr.adaming.model;

public enum EnumType {

	maison("Maison"), appartement("Appartement"), studio("Studio"), entrepot("Entrepot"), emplacement("Emplacement") , terrain("Terrain");

	private String displayName;

	private EnumType(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
