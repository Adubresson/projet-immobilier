package fr.adaming.model;

public enum EnumStatut {
	
	dispo("Disponible"), achete("Acheté"), loue("Loué");

	private String displayName;

	private EnumStatut(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
	
}
