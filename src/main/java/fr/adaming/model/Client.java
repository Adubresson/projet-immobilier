package fr.adaming.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="clients")
@XmlRootElement
@JsonIgnoreProperties({"listContrats", "listVisites"})
public class Client extends Personne {
	
	// déclaration des attributs
	private String telephone;
	
	// transformation de l'association uml en java
	@OneToMany(mappedBy="client")
	private List<Contrat> listContrats;
	@OneToMany(mappedBy="client")
	private List<Visite> listVisites;
	@ManyToOne
	@JoinColumn(name="a_id", referencedColumnName="id_a")
	private Agent agent;
	@ManyToMany(mappedBy="listClients",cascade = CascadeType.ALL)
	private List<ClasseStandard> listClassesStandards;

	// déclaration des constructeurs
	public Client() {
		super();
	}

	public Client(String nom, Adresse adresse, String telephone) {
		super(nom, adresse);
		this.telephone = telephone;
	}

	public Client(int id, String nom, Adresse adresse, String telephone) {
		super(id, nom, adresse);
		this.telephone = telephone;
	}

	// déclaration des getters et setters
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public List<Contrat> getListContrats() {
		return listContrats;
	}

	public void setListContrats(List<Contrat> listContrats) {
		this.listContrats = listContrats;
	}

	public List<Visite> getListVisites() {
		return listVisites;
	}

	public void setListVisites(List<Visite> listVisites) {
		this.listVisites = listVisites;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public List<ClasseStandard> getListClassesStandards() {
		return listClassesStandards;
	}

	public void setListClassesStandards(List<ClasseStandard> listClassesStandards) {
		this.listClassesStandards = listClassesStandards;
	}

	@Override
	public String toString() {
		return "Client [telephone=" + telephone + ", id=" + id + ", nom=" + nom + ", adresse=" + adresse + "]";
	}

}
