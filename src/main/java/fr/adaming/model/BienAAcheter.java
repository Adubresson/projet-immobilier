package fr.adaming.model;

import java.util.Date;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @Entity @DiscriminatorValue("ach")
 * @XmlRootElement
 **/
@Entity
@DiscriminatorValue("ach")
@XmlRootElement
@JsonIgnoreProperties({"contrat", "listVisites"})
public class BienAAcheter extends Bien {

	// Attributs
	private double prix;
	private String etat;

	@OneToMany(mappedBy = "baa")
	private List<Visite> listVisites;
	@OneToOne(mappedBy = "baa")
	private Contrat contrat;

	// Constructeurs
	public BienAAcheter() {
		super();
	}

	public BienAAcheter(int id) {
		super(id);
	}

	public BienAAcheter(String statut, Adresse adresse, String type, Date dateSoumission, Date dateDisponibilite,
			double revenuCadastral, double prix, String etat) {
		super(statut, adresse, type, dateSoumission, dateDisponibilite, revenuCadastral);
		this.prix = prix;
		this.etat = etat;
	}

	public BienAAcheter(int id, String statut, Adresse adresse, String type, Date dateSoumission,
			Date dateDisponibilite, double revenuCadastral, double prix, String etat) {
		super(id, statut, adresse, type, dateSoumission, dateDisponibilite, revenuCadastral);
		this.prix = prix;
		this.etat = etat;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public List<Visite> getListVisites() {
		return listVisites;
	}

	public void setListVisites(List<Visite> listVisites) {
		this.listVisites = listVisites;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	// Méthodes métiers
	@Override
	public String toString() {
		return "BienAAcheter [prix=" + prix + ", etat=" + etat + ", id=" + id + ", statut=" + statut + ", adresse="
				+ adresse + ", type=" + type + ", dateSoumission=" + dateSoumission + ", dateDisponibilite="
				+ dateDisponibilite + ", revenuCadastral=" + revenuCadastral + "]";
	}

}
