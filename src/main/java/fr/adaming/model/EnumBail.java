package fr.adaming.model;

public enum EnumBail {
	verbal("Bail verbal"), étudiant("Bail étudiant"), residPrincipal("Bail de résidence principale"),
	parking("Bail de parking"), locMeub("Bail de location meublée"), bureau("Bail de bureau"), loyer("Bail à loyer"),
	resSecondaire("Contrat de bail de résidence secondaire"), mixte("Bail mixte"), emphyt("Bail emphytéotique"),
	renov("Bail de rénovation"), locSais("Bail de location saisonnière"), coloc("Bail de colocation"),
	commercial("Bail commercial"), ferme("Bail à ferme");

	private String displayName;

	private EnumBail(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}
