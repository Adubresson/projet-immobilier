package fr.adaming.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="proprietaires")
@XmlRootElement
@JsonIgnoreProperties({"listBiensALouer","listBiensAAcheter"})
public class Proprietaire extends Personne {
	
	// déclaration des attributs
	private String telPrive;
	private String telTravail;
	
	// transformation de l'association uml en java
	@OneToMany(mappedBy="proprietaire")
	private List<BienALouer> listBiensALouer;
	@OneToMany(mappedBy="proprietaire")
	private List<BienAAcheter> listBiensAAcheter;
	
	// déclaration des constructeurs
	public Proprietaire() {
		super();
	}
	public Proprietaire(String nom, Adresse adresse, String telPrive, String telTravail) {
		super(nom, adresse);
		this.telPrive = telPrive;
		this.telTravail = telTravail;
	}
	public Proprietaire(int id, String nom, Adresse adresse, String telPrive, String telTravail) {
		super(id, nom, adresse);
		this.telPrive = telPrive;
		this.telTravail = telTravail;
	}
	
	// déclaration des getters et setters
	public String getTelPrive() {
		return telPrive;
	}
	public void setTelPrive(String telPrive) {
		this.telPrive = telPrive;
	}
	public String getTelTravail() {
		return telTravail;
	}
	public void setTelTravail(String telTravail) {
		this.telTravail = telTravail;
	}
	

	
	public List<BienALouer> getListBiensALouer() {
		return listBiensALouer;
	}
	public void setListBiensALouer(List<BienALouer> listBiensALouer) {
		this.listBiensALouer = listBiensALouer;
	}
	public List<BienAAcheter> getListBiensAAcheter() {
		return listBiensAAcheter;
	}
	public void setListBiensAAcheter(List<BienAAcheter> listBiensAAcheter) {
		this.listBiensAAcheter = listBiensAAcheter;
	}
	
	
	@Override
	public String toString() {
		return "Proprietaire [telPrive=" + telPrive + ", telTravail=" + telTravail + ", id=" + id + ", nom=" + nom
				+ ", adresse=" + adresse + "]";
	}
	
	

}
