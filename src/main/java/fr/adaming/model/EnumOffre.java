package fr.adaming.model;

public enum EnumOffre {
	aLouer("A louer"), aAcheter("A Acheter");

	private String displayName;

	private EnumOffre(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}
