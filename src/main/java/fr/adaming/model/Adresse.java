package fr.adaming.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Adresse implements Serializable {
	
	// déclaration des attributs
	private String rue;
	private String numero;
	private String cp;
	private String pays;
	
	// déclaration des constructeurs
	public Adresse() {
		super();
	}
	public Adresse(String rue, String numero, String cp, String pays) {
		super();
		this.rue = rue;
		this.numero = numero;
		this.cp = cp;
		this.pays = pays;
	}
	
	// déclaration des getters et setters
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	
	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", numero=" + numero + ", cp=" + cp + ", pays=" + pays + "]";
	}
	
	

}
