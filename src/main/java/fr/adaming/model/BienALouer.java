package fr.adaming.model;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
@Entity
@DiscriminatorValue("loc")
@XmlRootElement
**/
@Entity
@DiscriminatorValue("loc")
@XmlRootElement
@JsonIgnoreProperties({"contrat", "listVisites"})
public class BienALouer extends Bien {
	
	// Attributs
	private double caution;
	private double loyer;
	private double charges;
	private String typeBail;
	private String garniture;
	
	@OneToMany(mappedBy="bal")
	private List<Visite> listVisites;
	@OneToOne(mappedBy="baa")
	private Contrat contrat;

	public List<Visite> getListVisites() {
		return listVisites;
	}

	public void setListVisites(List<Visite> listVisites) {
		this.listVisites = listVisites;
	}

	// Constructeurs
	public BienALouer() {
		super();
	}
	
	public BienALouer(int id) {
		super(id);
	}
	
	public BienALouer(String statut, Adresse adresse, String type, Date dateSoumission,
			Date dateDisponibilite, double revenuCadastral, double caution, double loyer, double charges,
			String typeBail, String garniture) {
		super(statut, adresse, type, dateSoumission, dateDisponibilite, revenuCadastral);
		this.caution = caution;
		this.loyer = loyer;
		this.charges = charges;
		this.typeBail = typeBail;
		this.garniture = garniture;
	}	
	
	public BienALouer(int id, String statut, Adresse adresse, String type, Date dateSoumission,
			Date dateDisponibilite, double revenuCadastral, double caution, double loyer, double charges,
			String typeBail, String garniture) {
		super(id, statut, adresse, type, dateSoumission, dateDisponibilite, revenuCadastral);
		this.caution = caution;
		this.loyer = loyer;
		this.charges = charges;
		this.typeBail = typeBail;
		this.garniture = garniture;
	}

	// Setters & Getters
	public double getCaution() {
		return caution;
	}

	public void setCaution(double caution) {
		this.caution = caution;
	}

	public double getLoyer() {
		return loyer;
	}

	public void setLoyer(double loyer) {
		this.loyer = loyer;
	}

	public double getCharges() {
		return charges;
	}

	public void setCharges(double charges) {
		this.charges = charges;
	}

	public String getTypeBail() {
		return typeBail;
	}

	public void setTypeBail(String typeBail) {
		this.typeBail = typeBail;
	}

	public String getGarniture() {
		return garniture;
	}

	public void setGarniture(String garniture) {
		this.garniture = garniture;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	// Méthodes métiers
	@Override
	public String toString() {
		return "BienALouer [caution=" + caution + ", loyer=" + loyer + ", charges=" + charges + ", typeBail=" + typeBail
				+ ", garniture=" + garniture + ", id=" + id + ", statut=" + statut + ", adresse=" + adresse + ", type="
				+ type + ", dateSoumission=" + dateSoumission
				+ ", dateDisponibilite=" + dateDisponibilite + ", revenuCadastral=" + revenuCadastral + "]";
	}


	
	
	

}
