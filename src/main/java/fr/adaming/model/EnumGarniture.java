package fr.adaming.model;

public enum EnumGarniture {
	meuble("Meublé"), nonMeuble("Non Meublé");

	private String displayName;

	private EnumGarniture(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}
