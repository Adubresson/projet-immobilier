package fr.adaming.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="contrats")
@XmlRootElement
public class Contrat implements Serializable {
	
	// déclaration des attributs
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_co")
	private int id;
	private double prix;
	private Date date;
	private double fraisAgence;
	
	// transformation de l'association uml en java
	@ManyToOne
	@JoinColumn(name="cl_id", referencedColumnName="id_p")
	private Client client;
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="bal_id", referencedColumnName="id_b")
	private BienALouer bal;
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="baa_id", referencedColumnName="id_b")
	private BienAAcheter baa;
	
	// déclaration des constructeurs
	public Contrat() {
		super();
	}
	public Contrat(double prix, Date date, double fraisAgence) {
		super();
		this.prix = prix;
		this.date = date;
		this.fraisAgence = fraisAgence;
	}
	public Contrat(int id, double prix, Date date, double fraisAgence) {
		super();
		this.id = id;
		this.prix = prix;
		this.date = date;
		this.fraisAgence = fraisAgence;
	}
	
	// déclaration des getters et setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getFraisAgence() {
		return fraisAgence;
	}
	public void setFraisAgence(double fraisAgence) {
		this.fraisAgence = fraisAgence;
	}
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}

	
	
	
	public BienALouer getBal() {
		return bal;
	}
	public void setBal(BienALouer bal) {
		this.bal = bal;
	}
	public BienAAcheter getBaa() {
		return baa;
	}
	public void setBaa(BienAAcheter baa) {
		this.baa = baa;
	}
	
	@Override
	public String toString() {
		return "Contrat [id=" + id + ", prix=" + prix + ", date=" + date + ", fraisAgence=" + fraisAgence + "]";
	}
	
	

}
