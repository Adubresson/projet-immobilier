package fr.adaming.model;

public enum EnumEtat {
	aRestaurer("A Restaurer"), correct("Correct"), impec("Impeccable");

	private String displayName;

	private EnumEtat(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
	
}
