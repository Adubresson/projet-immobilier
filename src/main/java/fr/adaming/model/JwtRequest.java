package fr.adaming.model;

import java.io.Serializable;

public class JwtRequest implements Serializable {
	
	// cette classe sert de modele pour les requetes http recues
	
	// déclaration des attributs
	private String username;
	private String password;
	
	// déclaration des constructeurs
	public JwtRequest() {
		super();
	}
	public JwtRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	// déclaration des getters et setters
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
