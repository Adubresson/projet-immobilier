package fr.adaming.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="images")
public class Image {
	
	// attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Lob
	@JsonIgnore
	private byte[] photo;
	
	@Transient
	private String image;

	// constructeurs
	public Image() {
		super();
	}

	public Image(byte[] photo, String image) {
		super();
		this.photo = photo;
		this.image = image;
	}

	public Image(int id, byte[] photo, String image) {
		super();
		this.id = id;
		this.photo = photo;
		this.image = image;
	}

	// getters et setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	

}
