package fr.adaming.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {
	
	// cette classe sert de modele pour le body des reponses http
	
	// déclaration des attributs
	private final String jwttoken;

	// déclaration des constructeurs
	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	// déclaration du getter (pas de setter car l'attribut est final)
	public String getJwttoken() {
		return jwttoken;
	}

}
