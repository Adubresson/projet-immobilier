package fr.adaming.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="visites")
@XmlRootElement
public class Visite implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_v")
	private int id;
	private Date date;
	
	// transformation de l'association uml en java
	@ManyToOne
	@JoinColumn(name="cl_id", referencedColumnName="id_p")
	private Client client;
	@ManyToOne
	@JoinColumn(name="bal_id", referencedColumnName="id_b")
	private BienALouer bal;
	@ManyToOne
	@JoinColumn(name="baa_id", referencedColumnName="id_b")
	private BienAAcheter baa;

	public Visite(int id, Date date) {
		super();
		this.id = id;
		this.date = date;
	}

	public Visite() {
		super();
	}

	public Visite(Date date) {
		super();
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}



	public BienALouer getBal() {
		return bal;
	}

	public void setBal(BienALouer bal) {
		this.bal = bal;
	}

	public BienAAcheter getBaa() {
		return baa;
	}

	public void setBaa(BienAAcheter baa) {
		this.baa = baa;
	}

	@Override
	public String toString() {
		return "Visite [id=" + id + ", date=" + date + "]";
	}

}
