package fr.adaming.service;

import java.util.List;


import fr.adaming.model.BienALouer;

public interface IBienALouerService {

	public List<BienALouer> getAll();

	public BienALouer add(BienALouer bal);

	public BienALouer searchById(int id);

	public BienALouer update(BienALouer bal);

	public void delete(int id);
	
	public List<BienALouer> getBienImoAL(int idCs);

	public List<BienALouer> getBienImoALDispos(int idCs);
	
}
