package fr.adaming.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IAgentDao;
import fr.adaming.dao.IClasseStandardDao;
import fr.adaming.dao.IClientDao;
import fr.adaming.model.Agent;
import fr.adaming.model.BienAAcheter;
import fr.adaming.model.ClasseStandard;
import fr.adaming.model.Client;

@Service
public class ClientServiceImpl implements IClientService {

	// transformation de l'association uml en java
	@Autowired
	private IClientDao clDao;
	@Autowired
	private IAgentDao agDao;

	@Autowired
	private IClasseStandardDao csDao;

	@Override
	public List<Client> getAllClients() {
		return clDao.findAll();
	}

	@Override
	public Client getClientById(int id) {
		Optional<Client> clOpt = clDao.findById(id);
		return clOpt.get();
	}

	@Override
	public Client addClient(Client client) {

		List<ClasseStandard> liste = new ArrayList<ClasseStandard>();

		List<Client> listeCls = new ArrayList<Client>();

		listeCls.add(client);

		for (ClasseStandard cs : client.getListClassesStandards()) {

			Optional<ClasseStandard> csOpt = csDao.findById(cs.getId());

			cs = csOpt.get();

			cs.getListClients().addAll(listeCls);
			liste.add(cs);

		}

		client.setListClassesStandards(liste);
		// charger l'agent associé au client dans le contexte
		if (client.getAgent().getId() != 0) {
			Optional<Agent> agOpt = agDao.findById(client.getAgent().getId());
			client.setAgent(agOpt.get());
		} else {
			client.setAgent(null);
		}

		return clDao.save(client);
	}

	@Override
	public Client updateClient(Client client) {
		// charger l'agent associé au client dans le contexte
		if (client.getAgent().getId() != 0) {
			Optional<Agent> agOpt = agDao.findById(client.getAgent().getId());
			client.setAgent(agOpt.get());
		} else {
			client.setAgent(null);
		}

		return clDao.save(client);
	}

	@Override
	public void deleteClient(int id) {
		clDao.deleteById(id);

	}

	@Override
	public List<Client> getListClientByClS(int id) {
		List<Client> listCli = new ArrayList<Client>();
		System.out.println(clDao.getListClientByClS(id));

		for (Integer i : clDao.getListClientByClS(id)) {
			if (i != null) {
				Optional<Client> cOpt = clDao.findById(i);
				if (!cOpt.equals(Optional.empty())) {
					listCli.add(cOpt.get());
				}
			}
		}

		return listCli;
		
	}

}
