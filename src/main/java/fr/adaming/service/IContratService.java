package fr.adaming.service;

import java.util.List;

import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.Contrat;

public interface IContratService {

	public List<Contrat> getAllContrat();
	
	public Contrat getContratById(int id);
	
	public Contrat addContrat(Contrat contrat);
	
	public Contrat updateContrat(Contrat contrat);
	
	public void deleteContrat(int id);
	
	public List<BienALouer> getListBalAcquis(int id);
	
	public List<BienAAcheter> getListBaaAcquis(int id);
	
	public Double getRevenu(int id);
	
	
}
