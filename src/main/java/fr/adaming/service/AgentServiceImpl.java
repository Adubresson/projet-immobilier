package fr.adaming.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IAgentDao;
import fr.adaming.model.Agent;

@Service
public class AgentServiceImpl implements IAgentService {
	
	// transformation de l'association uml en java
	@Autowired
	private IAgentDao agDao;

	@Override
	public List<Agent> getAllAgents() {
		return agDao.findAll();
	}

	@Override
	public Agent getAgentById(int id) {
		Optional<Agent> agOpt = agDao.findById(id);
		return agOpt.get();
	}

	@Override
	public Agent addAgent(Agent agent) {
		return agDao.save(agent);
	}

	@Override
	public Agent updateAgent(Agent agent) {
		return agDao.save(agent);
	}

	@Override
	public void deleteAgent(int id) {
		agDao.deleteById(id);
		
	}

	@Override
	public Agent getAgentByLogin(String login) {
		return agDao.findByLogin(login);
	}

}
