package fr.adaming.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IBienAAcheterDao;
import fr.adaming.dao.IBienALouerDao;
import fr.adaming.dao.IClasseStandardDao;
import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.ClasseStandard;

@Service
public class ClasseStrandardServiceImpl implements IClasseStrandardService {

	// transformation de l'association iml en java
	@Autowired
	private IClasseStandardDao classeSDao;

	@Autowired
	private IBienAAcheterDao baaDao;
	
	@Autowired
	private IBienALouerDao balDao;
	
	@Override
	public List<ClasseStandard> getAllClasseStandard() {

		return classeSDao.findAll();
	}

	@Override
	public ClasseStandard getClasseStrandardById(int id) {

		Optional<ClasseStandard> classeSOps = classeSDao.findById(id);

		ClasseStandard cs= classeSOps.get();
		
		List<BienAAcheter> baa=baaDao.getBienImoAA(id);
		baa.forEach(System.out::println);
		
		List<BienALouer> bal=balDao.getBienImoAL(id);
		System.out.println("-------------------------------------");
		bal.forEach(System.out::println);
		cs.setListBAA(baa);
		
		cs.setListBAL(bal);
		
		return cs;
	}

	@Override
	public ClasseStandard addClasseStrandard(ClasseStandard classeS) {

		return classeSDao.save(classeS);
	}

	@Override
	public ClasseStandard updateClasseStrandard(ClasseStandard classeS) {

		return classeSDao.save(classeS);
	}

	@Override
	public void deleteClasseStrandard(int id) {
		classeSDao.deleteById(id);

	}

}
