package fr.adaming.service;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IBienAAcheterDao;
import fr.adaming.dao.IBienALouerDao;
import fr.adaming.dao.IClientDao;
import fr.adaming.dao.IContratDao;
import fr.adaming.model.Bien;
import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.Client;
import fr.adaming.model.Contrat;

@Service
public class ContratServiceImpl implements IContratService {

//transformation de l'association uml en jaava
	@Autowired
	private IContratDao contratDao;
	@Autowired
	private IClientDao clDao;
	@Autowired
	private IBienALouerDao balDao;
	@Autowired
	private IBienAAcheterDao baaDao;

	@Override
	public List<Contrat> getAllContrat() {

		return contratDao.findAll();
	}

	@Override
	public Contrat getContratById(int id) {

		Optional<Contrat> contObps = contratDao.findById(id);

		return contObps.get();
	}

	@Override
	public Contrat addContrat(Contrat contrat) {

		// charger les objets associés dans le contexte
		if (contrat.getClient().getId() != 0) {
			Optional<Client> clOpt = clDao.findById(contrat.getClient().getId());
			contrat.setClient(clOpt.get());
		} else {
			contrat.setClient(null);
		}

		if (contrat.getBal() != null && contrat.getBal().getId() != 0) {
			Optional<BienALouer> balOpt = balDao.findById(contrat.getBal().getId());
			BienALouer bal = balOpt.get();
			// modifier le statut du bien
			bal.setStatut("loué");
			// modifier le bien dans la bd
			balDao.save(bal);

			contrat.setBal(bal);
		}

		if (contrat.getBaa() != null && contrat.getBaa().getId() != 0) {
			Optional<BienAAcheter> baaOpt = baaDao.findById(contrat.getBaa().getId());
			BienAAcheter baa = baaOpt.get();
			// modifier le statut du bien
			baa.setStatut("acheté");
			// modifier le bien dans la bd
			baaDao.save(baa);
			contrat.setBaa(baa);
		}

		return contratDao.save(contrat);
	}

	@Override
	public Contrat updateContrat(Contrat contrat) {

		// charger les objets associés dans le contexte
		if (contrat.getClient().getId() != 0) {
			Optional<Client> clOpt = clDao.findById(contrat.getClient().getId());
			contrat.setClient(clOpt.get());
		} else {
			contrat.setClient(null);
		}

		if (contrat.getBal() != null && contrat.getBal().getId() != 0) {
			Optional<BienALouer> balOpt = balDao.findById(contrat.getBal().getId());
			BienALouer bal = balOpt.get();
			// modifier le statut du bien
			bal.setStatut("loué");
			// modifier le bien dans la bd
			balDao.save(bal);

			contrat.setBal(bal);
		}

		if (contrat.getBaa() != null && contrat.getBaa().getId() != 0) {
			Optional<BienAAcheter> baaOpt = baaDao.findById(contrat.getBaa().getId());
			BienAAcheter baa = baaOpt.get();
			// modifier le statut du bien
			baa.setStatut("acheté");
			// modifier le bien dans la bd
			baaDao.save(baa);
			contrat.setBaa(baa);
		}

		return contratDao.save(contrat);
	}

	@Override
	public void deleteContrat(int id) {

		contratDao.deleteById(id);

	}

	@Override
	public List<BienALouer> getListBalAcquis(int id) {
		List<BienALouer> listBal = new ArrayList<BienALouer>();

		for (Integer i : contratDao.getListBalAcquis(id)) {
			if (i != null) {
				Optional<BienALouer> eOpt = balDao.findById(i);
//				Contrat cOpt= contratDao.getContByIdBal(eOpt.get().getId());
				if (!eOpt.equals(Optional.empty())) {
//					BienALouer bOut = eOpt.get();
//					bOut.setContrat(cOpt);
					listBal.add(eOpt.get());
				}
			}
		}
		return listBal;
	}

	@Override
	public List<BienAAcheter> getListBaaAcquis(int id) {
		List<BienAAcheter> listBaa = new ArrayList<BienAAcheter>();
		System.out.println(contratDao.getListBaaAcquis(id));

		for (Integer i : contratDao.getListBaaAcquis(id)) {
			if (i != null) {
				Optional<BienAAcheter> eOpt = baaDao.findById(i);
//				Contrat cOpt= contratDao.getContByIdBaa(eOpt.get().getId());
				if (!eOpt.equals(Optional.empty())) {
//					BienAAcheter bOut = eOpt.get();
//					bOut.setContrat(cOpt);
					listBaa.add(eOpt.get());
				}
			}
		}

		return listBaa;
	}

	@Override
	public Double getRevenu(int id) {
		
		return contratDao.getRevenu(id);
	}


	
	

}
