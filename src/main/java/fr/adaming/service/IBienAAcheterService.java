package fr.adaming.service;

import java.util.List;

import fr.adaming.model.BienAAcheter;

public interface IBienAAcheterService {
	
	public List<BienAAcheter> getAll();

	public BienAAcheter add(BienAAcheter baa);

	public BienAAcheter searchById(int id);

	public BienAAcheter update(BienAAcheter baa);

	public void delete(int id);
	
	public List<BienAAcheter> getBienImoAA(int idCs);
	
	public List<BienAAcheter> getBienImoAADispos(int idCs);

}
