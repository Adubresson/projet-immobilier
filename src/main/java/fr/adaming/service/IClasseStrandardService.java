package fr.adaming.service;

import java.util.List;

import fr.adaming.model.ClasseStandard;

public interface IClasseStrandardService {

	public List<ClasseStandard> getAllClasseStandard();
	
	public ClasseStandard getClasseStrandardById (int id);
	
	public ClasseStandard addClasseStrandard (ClasseStandard classeS);
	
	public ClasseStandard updateClasseStrandard (ClasseStandard classeS);
	
	public void deleteClasseStrandard (int id);
	
	
}
