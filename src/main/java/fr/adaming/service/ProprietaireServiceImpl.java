package fr.adaming.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IProprietaireDao;
import fr.adaming.model.Proprietaire;

@Service
public class ProprietaireServiceImpl implements IProprietaireService {
	
	// transformation de l'association uml en java
	@Autowired
	private IProprietaireDao propDao;

	@Override
	public List<Proprietaire> getAllProprietaires() {
		return propDao.findAll();
	}

	@Override
	public Proprietaire getProprietaireById(int id) {
		Optional<Proprietaire> propOpt = propDao.findById(id);
		return propOpt.get();
	}

	@Override
	public Proprietaire addProprietaire(Proprietaire proprietaire) {
		return propDao.save(proprietaire);
	}

	@Override
	public Proprietaire updateProprietaire(Proprietaire proprietaire) {
		return propDao.save(proprietaire);
	}

	@Override
	public void deleteProprietaire(int id) {
		propDao.deleteById(id);
		
	}

}
