package fr.adaming.service;

import java.util.List;

import fr.adaming.model.Agent;

public interface IAgentService {
	
	public List<Agent> getAllAgents();
	
	public Agent getAgentById(int id);
	
	public Agent addAgent(Agent agent);
	
	public Agent updateAgent(Agent agent);
	
	public void deleteAgent(int id);
	
	public Agent getAgentByLogin(String login);

}
