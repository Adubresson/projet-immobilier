package fr.adaming.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IBienAAcheterDao;
import fr.adaming.dao.IClasseStandardDao;
import fr.adaming.dao.IProprietaireDao;
import fr.adaming.model.BienAAcheter;
import fr.adaming.model.ClasseStandard;
import fr.adaming.model.Image;
import fr.adaming.model.Proprietaire;

@Service
public class BienAAcheterServiceImpl implements IBienAAcheterService {

	// Transformation de l'association UML en Java
	@Autowired
	private IBienAAcheterDao baaDao;
	@Autowired
	private IProprietaireDao propDao;
	@Autowired
	private IClasseStandardDao csDao;

	@Override
	public List<BienAAcheter> getAll() {
		List<BienAAcheter> liste = baaDao.findAll();
		
		// convertir la photo (blob) de chaque bien en image (string)
		for (BienAAcheter bien : liste) {
			for (Image img : bien.getImages()) {
				img.setImage("data:image/png;base64," + Base64.encodeBase64String(img.getPhoto()));
			}
			
		}
		
		return liste;
	}

	@Override
	public BienAAcheter add(BienAAcheter baa) {
		// charger le propriétaire dans le contexte
		if (baa.getProprietaire().getId()!=0) {
			Optional<Proprietaire> propOpt = propDao.findById(baa.getProprietaire().getId());
			baa.setProprietaire(propOpt.get());
		} else {baa.setProprietaire(null);}
		

		// idem pour la classe standard
		if (baa.getClasseStandard().getId()!=0) {
			Optional<ClasseStandard> csOpt = csDao.findById(baa.getClasseStandard().getId());
			baa.setClasseStandard(csOpt.get());
		} else {baa.setClasseStandard(null);}
		
		return baaDao.save(baa);
	}

	@Override
	public BienAAcheter searchById(int id) {
		Optional<BienAAcheter> baaOps = baaDao.findById(id);
		
		if (!baaOps.equals(Optional.empty())) {
			BienAAcheter bien = baaOps.get();
			
			// convertir la photo (blob) en image (string)
			//bien.setImage("data:image/png;base64," + Base64.encodeBase64String(bien.getPhotos()));
			
			return baaOps.get();
		}
		
		return null;
	}

	@Override
	public BienAAcheter update(BienAAcheter baa) {
		return baaDao.save(baa);
	}

	@Override
	public void delete(int id) {
		baaDao.deleteById(id);
	}

	@Override
	public List<BienAAcheter> getBienImoAA(int idCs) {
		return baaDao.getBienImoAA(idCs);
	}

	@Override
	public List<BienAAcheter> getBienImoAADispos(int idCs) {
		return baaDao.getBienImoAADispos(idCs);
	}


}
