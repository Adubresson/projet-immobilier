package fr.adaming.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IBienAAcheterDao;
import fr.adaming.dao.IBienALouerDao;
import fr.adaming.dao.IClientDao;
import fr.adaming.dao.IVisiteDao;
import fr.adaming.model.BienAAcheter;
import fr.adaming.model.BienALouer;
import fr.adaming.model.Client;
import fr.adaming.model.Visite;

@Service
public class VisiteServiceImpl implements IVisiteService {

	// transformation UML en java
	@Autowired
	private IVisiteDao visiteDao;
	@Autowired
	private IClientDao clDao;
	@Autowired
	private IBienALouerDao balDao;
	@Autowired
	private IBienAAcheterDao baaDao;

	@Override
	public List<Visite> getAllVisite() {

		return visiteDao.findAll();
	}

	@Override
	public Visite getVisiteById(int id) {

		Optional<Visite> visiteOps = visiteDao.findById(id);

		return visiteOps.get();
	}

	@Override
	public Visite addVisite(Visite visite) {
		// charger les objets associés dans le contexte
		if (visite.getClient().getId()!=0) {
			Optional<Client> clOpt = clDao.findById(visite.getClient().getId());
			visite.setClient(clOpt.get());
		} else {visite.setClient(null);}
		
		if (visite.getBal()!=null && visite.getBal().getId()!=0) {
			Optional<BienALouer> balOpt = balDao.findById(visite.getBal().getId());
			visite.setBal(balOpt.get());
		}
		
		if (visite.getBaa()!=null && visite.getBaa().getId()!=0) {
			Optional<BienAAcheter> baaOpt = baaDao.findById(visite.getBaa().getId());
			visite.setBaa(baaOpt.get());
		}

		return visiteDao.save(visite);
	}

	@Override
	public Visite updateVisite(Visite visite) {
		// charger les objets associés dans le contexte
		if (visite.getClient().getId()!=0) {
			Optional<Client> clOpt = clDao.findById(visite.getClient().getId());
			visite.setClient(clOpt.get());
		} else {visite.setClient(null);}
		
		if (visite.getBal()!=null && visite.getBal().getId()!=0) {
			Optional<BienALouer> balOpt = balDao.findById(visite.getBal().getId());
			visite.setBal(balOpt.get());
		}
		
		if (visite.getBaa()!=null && visite.getBaa().getId()!=0) {
			Optional<BienAAcheter> baaOpt = baaDao.findById(visite.getBaa().getId());
			visite.setBaa(baaOpt.get());
		}
		
		
		return visiteDao.save(visite);
	}

	@Override
	public void deleteVisite(int id) {

		visiteDao.deleteById(id);

	}

	@Override
	public List<Visite> getVisitesByAgent(int idAgent) {
		return visiteDao.searchVisitesByAgent(idAgent);
	}

}
