package fr.adaming.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.adaming.dao.IBienALouerDao;
import fr.adaming.dao.IClasseStandardDao;
import fr.adaming.dao.IProprietaireDao;
import fr.adaming.model.BienALouer;
import fr.adaming.model.ClasseStandard;
import fr.adaming.model.Image;
import fr.adaming.model.Proprietaire;

@Service
public class BienALouerServiceImpl implements IBienALouerService {

	// Transformation de l'association UML en Java
	@Autowired
	private IBienALouerDao balDao;
	@Autowired
	private IProprietaireDao propDao;
	@Autowired
	private IClasseStandardDao csDao;
	
	@Override
	public List<BienALouer> getAll() {
		List<BienALouer> liste = balDao.findAll();
		// convertir les photos (blob) de chaque bien en images (string)
		for (BienALouer bien : liste) {
			for (Image img : bien.getImages()) {
				img.setImage("data:image/png;base64," + Base64.encodeBase64String(img.getPhoto()));
			}
			
		}
		
		return liste;
	}

	@Override
	public BienALouer add(BienALouer bal) {
		// charger le propriétaire dans le contexte
		if (bal.getProprietaire().getId()!=0) {
			Optional<Proprietaire> propOpt = propDao.findById(bal.getProprietaire().getId());
			bal.setProprietaire(propOpt.get());
		} else {bal.setProprietaire(null);}
		
		
		// idem pour la classe standard
		if (bal.getClasseStandard().getId()!=0) {
			Optional<ClasseStandard> csOpt = csDao.findById(bal.getClasseStandard().getId());
			bal.setClasseStandard(csOpt.get());
		} else {bal.setClasseStandard(null);}
		
		
		return balDao.save(bal);
	}

	@Override
	public BienALouer searchById(int id) {
		Optional<BienALouer> balOps = balDao.findById(id);
		if (!balOps.equals(Optional.empty())) {
			BienALouer bien = balOps.get();
			
			// convertir la photo (blob) en image (string)
			//bien.setImage("data:image/png;base64," + Base64.encodeBase64String(bien.getPhotos()));
			
			return bien;
		}
		return null;
	}

	@Override
	public BienALouer update(BienALouer bal) {
		return balDao.save(bal);
	}

	@Override
	public void delete(int id) {
		balDao.deleteById(id);	
	}

	@Override
	public List<BienALouer> getBienImoAL(int idCs) {
		
		return balDao.getBienImoAL(idCs);
	}

	@Override
	public List<BienALouer> getBienImoALDispos(int idCs) {
		return balDao.getBienImoALDispos(idCs);
	}


}
